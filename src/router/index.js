import Vue from 'vue'
import Router from 'vue-router'
import myrouter from '@/components/router'
import NotFound from '@/components/404'
import player from "@/components/player"
import testslot from "@/components/test/testslot"
import myalert from "@/components/alert"
import Animation from "@/components/Animation"
import magic from "@/components/magic"

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'myrouter',
      component: myrouter,
      children:[
        {
           path:'/404',
           name: '404',
          component: NotFound,
        },
        {
          path: '/testslot',
          name: 'testslot',
          component: testslot,
        },
        {
          path: '/player',
          name: 'player',
          component: player
        },
        {
          path: '/alert',
          name: 'alert',
          component: myalert,
        },
        {
          path: '/magic',
          name: 'magic',
          component: magic,
        },
        {
          path: '/animation',
          name: 'animation',
          component: Animation,
        }
      ]
    }
  ]
})
